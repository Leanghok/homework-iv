//
//  ViewController.swift
//  Homework-Note
//
//  Created by Hour Leanghok on 12/11/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var noteCollectionView: UICollectionView!
    
    var notes: [NSManagedObject] = []
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    @IBOutlet weak var addNoteLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noteCollectionView.register(UINib(nibName: "NoteCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "noteCollectionView")
        
        //Add long press gesture to collectionView
        noteCollectionView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(handelLongPress)))
        
        //Add gesture too take note label
        addNoteLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addNote)))
        
    }
    
    @objc func handelLongPress(gestureRecognizer: UILongPressGestureRecognizer){
        
        if (gestureRecognizer.state != UIGestureRecognizer.State.ended){
            return
        }
        let point = gestureRecognizer.location(in: self.noteCollectionView)
        let indexPath = self.noteCollectionView.indexPathForItem(at: point)
        
        let alert = UIAlertController(title: "Delete", message: "Are you sure to delete this note?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: "Delete Action"), style: .destructive, handler: { _ in
            self.deleteNote(id: self.notes[(indexPath?.row)!].value(forKey: "id") as! NSUUID)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel Action"), style: .cancel, handler: { _ in
            print("Cancel")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        notes = []
        getAllNote()
        noteCollectionView.reloadData()
    }
    
    func deleteAllNots(){
        let context = appDelegate.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "NOTES")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do{
            let result = try context.execute(request)
            print(result)
        }catch{
            print(error)
        }
    }
    
    func deleteNote(id: NSUUID){
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "NOTES")
        do{
            let data = try context.fetch(fetchRequest)
            
            for note in data as! [NOTES] {
                if((note.value(forKey: "id") as! NSUUID) == id){
                    
                    context.delete(note)
                    
                    do{
                        try context.save()
                        viewWillAppear(false)
                    }catch{
                        print(error)
                    }
                }
            }
            
        }catch{
            print(error)
        }
    }
    
    func getAllNote(){
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "NOTES")
        request.returnsObjectsAsFaults = false
        do{
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject]{
                notes.append(data)
            }
        }catch {
            print("Error can not fetch data!")
        }
    }
    
    @objc func addNote(){
        let selectionVC = storyboard?.instantiateViewController(withIdentifier: "addNoteVC") as! AddNoteViewController
        selectionVC.isUpdateNote = false
        show(selectionVC, sender: nil)
    }
    
    
    @IBAction func microphoneButtonTap(_ sender: UIButton) {
        addNote()
    }
    
    
    @IBAction func penButtonTap(_ sender: UIButton) {
        addNote()
    }
    
    @IBAction func listButtonTap(_ sender: UIButton) {
        addNote()
    }
    
    @IBAction func changeLanguageButtonTap(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "Language", message: "Choose langugage", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "English", style: .default, handler: { _ in
            print("English Selected")
        }))
        
        alert.addAction(UIAlertAction(title: "Khmer", style: .default, handler: { _ in
            print("Khmer Selected")
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            print("Canccel")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension ViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "noteCollectionView", for: indexPath) as! NoteCollectionViewCell
        cell.titleLabel.text = notes[indexPath.row].value(forKey: "title") as? String
        cell.noteLabel.text = notes[indexPath.row].value(forKey: "note") as? String
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  10
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectionVC = storyboard?.instantiateViewController(withIdentifier: "addNoteVC") as! AddNoteViewController
        selectionVC.isUpdateNote = true
        selectionVC.noteTitle = notes[indexPath.row].value(forKey: "title") as? String ?? ""
        selectionVC.noteContent = notes[indexPath.row].value(forKey: "note") as? String ?? ""
        selectionVC.noteID = notes[indexPath.row].value(forKey: "id") as? NSUUID ?? nil
        show(selectionVC, sender: nil)
    }
    
    
    
    
}

