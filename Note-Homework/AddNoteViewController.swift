//
//  AddNoteViewController.swift
//  Note-Homework
//
//  Created by Hour Leanghok on 12/11/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit
import CoreData

class AddNoteViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var noteTextView: UITextView!
    
    var isUpdateNote: Bool = false
    
    var noteID:NSUUID?
    
    var noteTitle: String = ""
    
    var noteContent: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isUpdateNote == false{
            //Add note
        }else{
            titleTextField.text = noteTitle
            noteTextView.text = noteContent
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isUpdateNote == false{
            addNote()
        }else{
            updateNote()
        }
    }
    
    func addNote() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let id = NSUUID()
        let context = appDelegate.persistentContainer.viewContext
        
        let note = NOTES(context: context)
        note.setValue(id, forKey: "id")
        note.setValue(titleTextField.text, forKey: "title")
        note.setValue(noteTextView.text, forKey: "note")
        appDelegate.saveContext()
    }
    
    func updateNote() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "NOTES")
        do{
            let data = try context.fetch(fetchRequest)
            
            for note in data as! [NSManagedObject] {
                if((note.value(forKey: "id") as! NSUUID) == noteID){
                    note.setValue(titleTextField.text, forKey: "title")
                    note.setValue(noteTextView.text, forKey: "note")
                    do{
                        try context.save()
                        print("Updated")
                    }catch{
                        print(error)
                    }
                }
            }
            
        }catch{
            print(error)
        }
    }
    
    @IBAction func detailButtonTap(_ sender: UIButton) {
        addAlert()
    }
    
    @IBAction func plusButtontap(_ sender: UIButton) {
        addAlert()
    }
    
    func addAlert(){
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Take photo", style: .default, handler: { _ in
            print("English Selected")
        }))
        
        alert.addAction(UIAlertAction(title: "Choose image", style: .default, handler: { _ in
            print("Khmer Selected")
        }))
        
        alert.addAction(UIAlertAction(title: "Drawing", style: .default, handler: { _ in
            print("Khmer Selected")
        }))
        
        alert.addAction(UIAlertAction(title: "Recording", style: .default, handler: { _ in
            print("Khmer Selected")
        }))
        
        alert.addAction(UIAlertAction(title: "Checkboxs", style: .default, handler: { _ in
            print("Khmer Selected")
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            print("Canccel")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
