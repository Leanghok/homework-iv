//
//  NoteCollectionViewCell.swift
//  Homework_IV
//
//  Created by Hour Leanghok on 12/11/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit

class NoteCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var noteLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
