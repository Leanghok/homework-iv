//
//  Language Manager.swift
//  Note-Homework
//
//  Created by Hour Leanghok on 12/11/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation

class LanguageManager{
    
    var language: String {
        get{
            return UserDefaults.standard.string(forKey: "language") ?? "en"
        }
        set{
            UserDefaults.standard.setValue("km", forKey: "language")
        }
    }
    
}
